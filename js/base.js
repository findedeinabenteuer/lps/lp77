function findGetParameter(parameterName) {
    var result = null,
    tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
    return result;
}

$(document).ready(function () {

    var aid = findGetParameter("aid");
    var pid = findGetParameter("pid");
    var cid = findGetParameter("cid");

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');
        allBackBtn = $('.backBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allBackBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            backStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        backStepWizard.removeAttr('disabled').trigger('click');
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'], select"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        $(".form-group").removeClass("has-success");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            } else {
                $(curInputs[i]).closest(".form-group").addClass("has-success");
            }
        }

        curInputs.each(function() {
            $(this).change(function() {
                if (!$(this)[0].validity.valid) {
                    $(this).closest(".form-group").addClass("has-error");
                } else {
                    $(this).closest(".form-group").removeClass("has-error").addClass("has-success");
                }
            })
        })

        if (isValid) {
            if($(this).hasClass("api-check-btn")) {

                // Add Loader
                $(".center-parent").show();

                var dhis = $(this);
                $(this).parent().find(".api-check").trigger("change");

                setTimeout(function() {
                    $(".center-parent").hide();
                    if(dhis.parent().find(".has-error").length == 0) {
                        nextStepWizard.removeAttr('disabled').trigger('click');        
                    }
                    
                },2000)

            } else {
                nextStepWizard.removeAttr('disabled').trigger('click');
            }

            
        }
        
    });

    $('div.setup-panel div a.btn-success').trigger('click');




    $(".api-check-finish-btn").on("click", function() {

        username = $(".api-username").val();
        passwort = $(".api-passwort").val();
        email = $(".api-email").val();
        geburtstag = $(".api-dob").val();
        plz = $(".api-zip").val();
        land = $(".api-land").val();
  
      data = {
          "email": email,
          "password": passwort,
          "profile_data": {
              "nickname": username,
              "canswers": {
                  "birth": geburtstag,
                  "country": land,
                  "zip": plz
              }
          },
          "traffic_partner_id": pid,
          "source_id": aid,
          "click_id": cid
      };
      
  
      $.ajax({
        beforeSend: function () {
          $(".center-parent").show();
        },
        contentType: "application/json",
        type: "POST",
        dataType: "json",
        url: "https://main.fda-backend.de/api/auth/register_external",
        data: JSON.stringify(data),
        success: function (data) {
          $(".center-parent").hide();
          if (data.valid == 1 || data.login_url.length>0) {
            window.location.replace(data.login_url);
          } else {
            reloadPage();
          }
        },
        error: function (error, status) {
            $(".center-parent").hide();
            $('.submit-error').html('Fehler: ' + error.responseJSON.message);
            $(".submit-error").show();
          
        },
      });
  
    });

});